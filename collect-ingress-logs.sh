#!/bin/bash

######
# Script to collect ingress-nginx logs from remote
# Needs:
# - ip addresses to be defined
# - rsync
# - ssh access via ssh key
#
# Some defaults to consider:
# - INI file is at ${SCRIPT_PATH}/collect-ingress-logs.ini
#####

debug_it=true

# Script full path
# https://unix.stackexchange.com/questions/17499/get-path-of-current-script-when-executed-through-a-symlink/17500
SCRIPT_PATH="$(dirname "$(readlink -f "$0")")"
SCRIPT_NAME=$(basename $0)
DATENOW=$(date +%y%m%d_%H%M%S)

function usage()
{
  shopt -s xpg_echo
  echo "Usage: $0 <options> \n
  Options:
  -h|--help \t\t the help message
  -i|--ips \t input file with IPs (default: ip-addresses.txt)
  -k|--key \t full path to the SSH key" 1>&2; exit 0;
}

### define defaults
# load following defaults from the collect-ingress-logs.ini:
# - sync_root
# - ingress_log_path_remote
# - remoter_user
# - ssh_key
source ${SCRIPT_PATH}/collect-ingress-logs.ini
# file with IP addresses to sync
ips_file="${SCRIPT_PATH}/ip-addresses.txt"
# where to store log messages of the script itself
script_log_file="${sync_root}/${SCRIPT_NAME}-log.txt"
script_error_file="${sync_root}/${SCRIPT_NAME}-err.txt"

# create sync_root directory, if does not exist
[[ ! -d "${sync_root}" ]] && mkdir -p "${sync_root}"

function check_arguments()
{
  OPTIONS=hik:
  LONGOPTS=help,ips,key:
  # https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash
  # saner programming env: these switches turn some bugs into errors
  set -o errexit -o pipefail -o noclobber -o nounset
  #set  +o nounset
  ! getopt --test > /dev/null
  if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo '`getopt --test` failed in this environment.'
    exit 1
  fi

  # -use ! and PIPESTATUS to get exit code with errexit set
  # -temporarily store output to be able to check for errors
  # -activate quoting/enhanced mode (e.g. by writing out "--options")
  # -pass arguments only via   -- "$@"   to separate them correctly
  ! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
  if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # e.g. return value is 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
  fi
  # read getopt's output this way to handle the quoting right:
  eval set -- "$PARSED"

  if [ "$1" == "--" ]; then
    echo "[INFO] No arguments provided. Using defaults" >>"${script_log_file}" 2>&1
  fi

  # now enjoy the options in order and nicely split until we see --
  while true; do
    case "$1" in
        -h|--help)
            usage
            shift
            ;;
        -i|--ips)
            ips_file="$2"
            shift
            ;;
        -k|--key)
            ssh_key="$2"
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            break
            ;;
    esac
  done
}


function sync_logs()
{
  ip=$1
  ingress_log_path="${sync_root}/${ip}"
  [[ ! -d "${ingress_log_path}" ]] && mkdir -p "${ingress_log_path}"
  echo "Synchronising ingress-nginx logs from ${ip}" >> "${script_log_file}"
  rsync -avz -e "ssh -i ${ssh_key}" --progress "${remote_user}@${ip}:${ingress_log_path_remote}/" "${ingress_log_path}" >>"${script_log_file}" 2>&1
}

echo " " >>"${script_log_file}"
echo "${DATENOW}:" >>"${script_log_file}" 2>&1

check_arguments "$0" "$@"

while IFS= read -r ip
do
  # sync, if copying from some IP fails, continue to next IP
  sync_logs "$ip" || continue
done < "${ips_file}"
