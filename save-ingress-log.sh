#!/bin/bash

#####
# Script to regulary store/backup ingress-nginx log of kubernetes
# expected namespace: ingress-nginx
# place to store: /var/log/ingress-nginx
#
# where to run: k8s system
#####

set -e

### defaults ###
# kubernetes namespace for ingress
namespace="ingress-nginx"

# Directory where to store logs
LOG_DIR="/var/log/ingress-nginx"
[[ ! -d "${LOG_DIR}" ]] && mkdir -p "${LOG_DIR}"

# script its own name
SCRIPT_NAME=$(basename $0)
# script logfile
script_logfile="${LOG_DIR}/${SCRIPT_NAME}.log.txt"
###

# Current date
DATENOW=$(date +%y%m%d_%H%M%S)

# Get current ingress-nginx pod:
ingress_pod=$(kubectl get pods --no-headers -o custom-columns=":metadata.name" -n ${namespace} |grep -i controller)

ingress_logfile_tmp="${LOG_DIR}/${ingress_pod}-log.tmp"
ingress_logfile="${LOG_DIR}/${ingress_pod}-log.txt"
size_of_ingress_logfile=0
size_of_ingress_logfile_tmp=0

# read current ingress log into the file
if [ ${#ingress_pod} -ge 1 ]; then
  ingress_log=$(kubectl logs ${ingress_pod} -n ${namespace} >${ingress_logfile_tmp})
  size_of_ingress_logfile_tmp=$(stat -c%s "${ingress_logfile_tmp}")
fi

# check if ingress logfile already exists, check its size
[[ -f "${ingress_logfile}" ]]  && size_of_ingress_logfile=$(stat -c%s "$ingress_logfile")

if [ $size_of_ingress_logfile_tmp -ge $size_of_ingress_logfile ]; then
  # copy .tmp into .txt, if it is bigger
  mv ${ingress_logfile_tmp} ${ingress_logfile}
  LOG_MESSAGE="${DATENOW}: saved in ${ingress_logfile}, size is ${size_of_ingress_logfile_tmp}"
else
  # if smaller, rename old file by adding datenow
  # copy new .tmp into .txt
  mv ${ingress_logfile} "${ingress_logfile}.${DATENOW}"
  mv ${ingress_logfile_tmp} ${ingress_logfile}
  LOG_MESSAGE="${DATENOW}: the previous log saved in ${ingress_logfile}.${DATENOW}; new one saved in ${ingress_logfile}, size is ${size_of_ingress_logfile_tmp}"
fi

echo $LOG_MESSAGE >> ${script_logfile}
