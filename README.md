# collect-ingress-logs

Scripts to:
 * regulary store/backup ingress-nginx log of kubernetes
 * collect these ingress-nginx logs from (remote) kubernetes
 * convert them into HTML using goaccess

