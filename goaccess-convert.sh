#!/bin/bash

#####
# Script to convert nginx logfile to HTML by means of goaccess
# Needs goaccess to be installed
#
# place to run: where goaccess is installed and logfiles are stored (e.g. k8s or "local")
#####

USAGEMESSAGE="Usage: $0 { --help | nginx_logfile | nginx_logfile --html } \n
              --help       this message \n
              --html       generate HTML output"


# default value
create_html="false"

## Check correctness of the script call #
arr=("$@")
if [ $# -eq 0 ] || [ $1 == "-h" ] || [ $1 == "--help" ]; then
    shopt -s xpg_echo
    echo $USAGEMESSAGE
    exit 1
elif [ $# -eq 1 ]; then
    logfile=$1
elif [ $# -ge 2 ] && [ $# -le 3 ]; then
    logfile=$1
    [[ $2 = *"--html"* ]]  && create_html="true"
else
    echo "ERROR! Too many arguments provided!"
    shopt -s xpg_echo
    echo $USAGEMESSAGE
    exit 2
fi

output="${logfile}.html"
#goaccess -f ${logfile} --ignore-crawlers --log-format=COMBINED -a -d --geoip-database $HOME/GeoIP/GeoLite2-Country.mmdb -o ${output}

[[ "$create_html" == "true" ]] && goaccess -f ${logfile} --ignore-crawlers --log-format=COMBINED -a -d -o ${output} && echo "[INFO] HTML Output is written to ${output}"
[[ "$create_html" == "false" ]] && goaccess -f ${logfile} --ignore-crawlers --log-format=COMBINED -a -d
